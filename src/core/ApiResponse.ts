/* eslint-disable prettier/prettier */
import { Response } from 'express';
import { PaginationModel } from 'mongoose-paginate-ts';
import Quiz from '../database/models/Quiz';

enum ResponseStatus {
  SUCCESS = 200,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  VALIDATION_ERROR = 402,
  INTERNAL_ERROR = 500,
}

abstract class ApiResponse {
  protected constructor(
      protected statusCode: ResponseStatus,
      protected status: ResponseStatus,
      protected message: string,
  ) {
  }

  protected prepare<T extends ApiResponse>(res: Response, response: T): Response {
    return res.status(this.status).json(ApiResponse.sanitize(response));
  }

  public send(res: Response): Response {
    return this.prepare<ApiResponse>(res, this);
  }

  private static sanitize<T extends ApiResponse>(response: T): T {
    const clone: T = {} as T;
    Object.assign(clone, response);
    // @ts-ignore
    delete clone.status;
    for (const i in clone) if (typeof clone[i] === 'undefined') delete clone[i];
    return clone;
  }
}
// Constructs a type by picking the set of properties Keys (string literal or union of string literals) from Type
type MetaData = Pick<PaginationModel<Quiz>,
  'totalDocs' | 'limit' | 'totalPages' | 'page' | 'pagingCounter' | 'hasPrevPage' | 'hasNextPage' | 'prevPage' | 'nextPage' | 'hasMore'>;

export class ApiResponseDataMsg {
  private statusCode;
  private readonly message;
  private readonly data;
  private readonly meta;

  constructor(message: string, statusCode: ResponseStatus, data: Array<Object>, meta: MetaData) {
    this.statusCode = statusCode;
    this.message = message;
    this.data = data;
    this.meta = meta;
  }

  send(res: Response): Response {
    return res
      .status(this.statusCode)
      .json({ message: this.message, data: this.data, meta: this.meta });
  }
}
export class SuccessResponsePagination extends ApiResponseDataMsg{

  constructor(message: string, data: Array<Object>, meta: MetaData) {
    super(message, ResponseStatus.SUCCESS, data, meta);
  }
}
export class AuthFailureResponse extends ApiResponse {
  constructor(message = 'Authentication Failure') {
    super(ResponseStatus.UNAUTHORIZED, ResponseStatus.UNAUTHORIZED, message);
  }
}

export class NotFoundResponse extends ApiResponse {
  private url: string | undefined;

  constructor(message = 'Not Found') {
    super(ResponseStatus.NOT_FOUND, ResponseStatus.NOT_FOUND, message);
  }

  send(res: Response): Response {
    this.url = res.req?.originalUrl;
    return super.prepare<NotFoundResponse>(res, this);
  }
}

export class ForbiddenResponse extends ApiResponse {
  constructor(message = 'Forbidden') {
    super(ResponseStatus.FORBIDDEN, ResponseStatus.FORBIDDEN, message);
  }
}

export class BadRequestResponse extends ApiResponse {
  constructor(message = 'Bad Parameters') {
    super(ResponseStatus.BAD_REQUEST, ResponseStatus.BAD_REQUEST, message);
  }
}
export class InternalErrorResponse extends ApiResponse {
  constructor(message = 'Internal Error') {
    super(ResponseStatus.INTERNAL_ERROR, ResponseStatus.INTERNAL_ERROR, message);
  }
}
export class ValidationErrorResponse extends ApiResponse {
  constructor(message = 'Validation Error') {
    super(ResponseStatus.VALIDATION_ERROR, ResponseStatus.VALIDATION_ERROR, message);
  }
}

export class SuccessMsgResponse extends ApiResponse {
  constructor(message: string) {
    super(ResponseStatus.SUCCESS, ResponseStatus.SUCCESS, message);
  }
}

export class FailureMsgResponse extends ApiResponse {
  constructor(message: string) {
    super(ResponseStatus.SUCCESS, ResponseStatus.SUCCESS, message);
  }
}

export class SuccessResponse<T> extends ApiResponse {
  constructor(message: string, private data: T) {
    super(ResponseStatus.SUCCESS, ResponseStatus.SUCCESS, message);
  }

  send(res: Response): Response {
    return super.prepare<SuccessResponse<T>>(res, this);
  }
}

export class AccessTokenErrorResponse extends ApiResponse {
  private instruction = 'refresh_token';

  constructor(message = 'Access token invalid') {
    super(ResponseStatus.UNAUTHORIZED, ResponseStatus.UNAUTHORIZED, message);
  }

  send(res: Response): Response {
    res.setHeader('instruction', this.instruction);
    return super.prepare<AccessTokenErrorResponse>(res, this);
  }
}

export class TokenRefreshResponse extends ApiResponse {
  constructor(message: string, private accessToken: string, private refreshToken: string) {
    super(ResponseStatus.SUCCESS, ResponseStatus.SUCCESS, message);
  }

  send(res: Response): Response {
    return super.prepare<TokenRefreshResponse>(res, this);
  }
}
