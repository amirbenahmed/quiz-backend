import { describe, expect } from '@jest/globals';
import request from 'supertest';
import app from '../../app';
import { connectToTestingDb, disconnect } from '../../database';
import { Types } from 'mongoose';
const quizId = new Types.ObjectId()
const questionId = new Types.ObjectId()
let createdQuizId = new Types.ObjectId()
let createdQuestionId = new Types.ObjectId()
describe('failure questions endpoints ', () => {
  beforeAll(()=> connectToTestingDb())
  afterAll( () => disconnect())
  it('should return an error in POST if the question already exist in database', async () => {
    const response = await request(app).post('/api/v1/quizzes').send({
      title: 'amirosben',
      description: 'description of the quiz',
    });
    expect(response.status).toEqual(200);
    createdQuizId = response.body.data.quiz._id;


    const question = await request(app)
      .post(`/api/v1/quizzes/${createdQuizId}/questions`)
      .send({
        title: "What is the capital of Tunisia?",
        description: "I need to know for my geography homework.",
        audio: "http://www.audiowebsite.com/history/warTwo.mp3",
        videolink: "http://www.videowebsite.com/history/warTwoRecap",
        multipleChoice: false,
        answers: [],
      });
    expect(question.status).toBe(200);
    createdQuestionId = question.body.data.question._id;

    const res = await request(app)
      .post(`/api/v1/quizzes/${createdQuizId}/questions`)
      .send({
        title: "What is the capital of Tunisia?",
        description: "I need to know for my geography homework.",
        audio: "http://www.audiowebsite.com/history/warTwo.mp3",
        videolink: "http://www.videowebsite.com/history/warTwoRecap",
        multipleChoice: false,
        answers: [],

      });
    expect(res.status).toBe(400);
    expect(res.body).toMatchObject({ message: 'this question already exists', statusCode: 400 })
  });

  it('should return an error in for GET if the question does not exist in database', async () => {
    const res = await request(app).get(`/api/v1/quizzes/${quizId}/questions/${questionId}`);
    expect(res.status).toBe(400);
    expect(res.body).toMatchObject({ message: 'this question does not belong to the quiz', statusCode: 400 })
  });

  it('should return an error in for PUT if the question does not exist in database', async () => {
    const res = await request(app)
      .put(`/api/v1/quizzes/${quizId}/questions/${createdQuestionId}`)
      .send({
        title: 'What is the capital of Tunisia?',
        description: 'I need to know for my geography homework.',
        audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
        videolink:'http://www.videowebsite.com/history/warTwoRecap',
        multipleChoice: false,
      });
    expect(res.status).toBe(400);
    expect(res.body).toMatchObject({ message: 'this question does not belong to the quiz', statusCode: 400 })
  });

  it('should return an error in for DELETE if the question does not exist in database', async () => {
    const res = await request(app).delete(`/api/v1/quizzes/${quizId}/questions/${questionId}`);
    expect(res.status).toBe(400);
    expect(res.body).toMatchObject({ message: 'this question does not belong to the quiz', statusCode: 400 })
  });


  it('should return an error for Joi validation', async () => {
    const res = await request(app)
      .post(`/api/v1/quizzes/${createdQuizId}/questions`)
      .send({
        description: 'I need to know for my geography homework.',
        audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
        videolink:'http://www.videowebsite.com/history/warTwoRecap',
        multipleChoice:false,
      });
    expect(res.status).toBe(402);
    expect(res.body).toMatchObject({ message: 'title is required', statusCode: 402 })
  });

  it('should return an error for Joi validation in minimum characters case', async () => {
    const res = await request(app)
      .put(`/api/v1/quizzes/${createdQuizId}/questions/${questionId}`)
      .send({
        title:'a',
        description: 'I need to know for my geography homework.',
        audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
        videolink:'http://www.videowebsite.com/history/warTwoRecap',
        multipleChoice: false,
      });
    expect(res.status).toBe(402);
    expect(res.body).toMatchObject({ message: 'title length must be at least 3 characters long', statusCode: 402 })
  });
});
