import request from 'supertest';
import app from '../../app';
import  { Types } from 'mongoose';
import { expect } from '@jest/globals';
import { connectToTestingDb, disconnect } from '../../database';

describe('success questions endpoints', () => {
  beforeAll(()=> connectToTestingDb())
  afterAll( () => disconnect())
  let quizId: Types.ObjectId;
  let questionId: Types.ObjectId;

  it('should create a quiz ', async () => {
    const response = await request(app).post('/api/v1/quizzes').send({
      title: 'technologies',
      description: 'description of the quiz',
    });

    expect(response.status).toEqual(200);
    quizId = response.body.data.quiz._id;
    expect(response.body.data.quiz).toMatchObject({
      _id: expect.any(String),
      title: 'technologies',
      description: 'description of the quiz',
      createdAt: expect.any(String),
      isPublished: false,
      updatedAt: expect.any(String),
    });
  });

  it('should create a question', async () => {
    const createResponse = await request(app)
      .post(`/api/v1/quizzes/${quizId}/questions`)
      .send({
        title: 'What is the weather?',
        description: 'I need to know for my geography homework.',
        audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
        videolink:'http://www.videowebsite.com/history/warTwoRecap',
        multipleChoice: false,
      });

    expect(createResponse.status).toEqual(200);
    expect(createResponse.body.data.question).toMatchObject({
      _id: expect.any(String),
      title: 'What is the weather?',
      description: 'I need to know for my geography homework.',
      audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
      videolink:'http://www.videowebsite.com/history/warTwoRecap',
      multipleChoice:false,
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
    });
    questionId = createResponse.body.data.question._id;
  });

  it('reads a question', async () => {
    const response = await request(app).get(`/api/v1/quizzes/${quizId}/questions/${questionId}`);

    expect(response.status).toBe(200);
    expect(response.body.data).toMatchObject({
      title: 'What is the weather?',
      description: 'I need to know for my geography homework.',
      audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
      videolink:'http://www.videowebsite.com/history/warTwoRecap',
      multipleChoice: false,
    });
  });

  it('updates a question', async () => {
    const response = await request(app)
      .put(`/api/v1/quizzes/${quizId}/questions/${questionId}`)
      .send({
        title: 'What is the capital of Tunisia?',
        description: 'I need to know for my geography test.',
        audio: 'http://www.audiowebsite.com/technolgy/machinelearning.mp3',
        videolink:'http://www.videowebsite.com/history/introMachineLearning',
        multipleChoice: true,
      });

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ message: 'Question updated successfully' })
    ;
  });

  it('deletes a question', async () => {
    const response = await request(app).delete(`/api/v1/quizzes/${quizId}/questions/${questionId}`);

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({
      message: 'Question Deleted',
      statusCode: 200,
    });
  })
});
