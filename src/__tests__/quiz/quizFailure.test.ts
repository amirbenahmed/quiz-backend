import request from 'supertest';
import { describe, expect } from '@jest/globals';
import app from '../../app';
import { connectToTestingDb, disconnect } from '../../database';
import { Types } from 'mongoose';
const quizId = new Types.ObjectId();


describe('failure quiz endpoints ', () => {
  beforeAll(() => connectToTestingDb());
  afterAll(() => disconnect());
  it('should return an error if creating a quiz already exist', async () => {
    const res = await request(app).post('/api/v1/quizzes').send({
      title: 'techno',
      description: 'description of the quiz',
    });
    expect(res.status).toBe(400);
    expect(res.body).toMatchObject({ message: 'this quiz already exists', statusCode: 400 });
  });
  it('should return an error if retrieving a quiz does not exist in database', async () => {
    const res = await request(app).get(`/api/v1/quizzes/${quizId}`);
    expect(res.status).toBe(404);
    expect(res.body).toMatchObject({ message: 'Quiz not found or deleted', statusCode: 404 });
  });

  it('should return an error if updating a quiz with an invalid ID', async () => {
    const res = await request(app).put(`/api/v1/quizzes/${quizId}`).send({
      title: 'Updated Test Quiz',
      description: 'This is an updated test quiz',
    });
    expect(res.status).toBe(404);
    expect(res.body).toMatchObject({ message: 'Quiz not found or deleted', statusCode: 404 });
  });

  it('should return an error if deleting a quiz with an invalid ID', async () => {
    const res = await request(app).delete(`/api/v1/quizzes/${quizId}`);
    expect(res.status).toBe(404);
    expect(res.body).toMatchObject({ message: 'Quiz not found or deleted', statusCode: 404 });
  });
  it('should return an error if id contains an invalid value', async () => {
    const res = await request(app).patch(`/api/v1/quizzes/638cb784fecf5a1353d9c590`);
    expect(res.status).toBe(404);
    expect(res.body).toMatchObject({ statusCode: 404, message: 'Quiz not found or deleted' });
  });
});
it('should return an error for Joi validation', async () => {
  const res = await request(app).post('/api/v1/quizzes').send({
    description: 'description of the quiz',
  });
  expect(res.status).toBe(402);
  expect(res.body).toMatchObject({ message: 'title is required', statusCode: 402 });
});
it('should return an error for Joi validation', async () => {
  const res = await request(app).put(`/api/v1/quizzes/${quizId}`).send({
    isPublished: true
  });
  expect(res.status).toBe(402);
  expect(res.body).toMatchObject({ message: 'title is required', statusCode: 402 });
});
// here for testing get all quizzes i think it need only testing for authorization later
