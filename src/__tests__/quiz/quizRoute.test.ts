import request from 'supertest';
import { describe, expect } from '@jest/globals';
import { Types } from 'mongoose';
import app from '../../app';
import { connectToTestingDb, disconnect } from '../../database';

describe('success quiz endpoints ', () => {
  beforeAll(()=> connectToTestingDb())
  afterAll( () => disconnect())

  let createdQuizId: Types.ObjectId;

  it('should create a quiz ', async () => {
    const response = await request(app).post('/api/v1/quizzes').send({
      title: 'techno',
      description: 'description of the quiz',
    });

    expect(response.status).toEqual(200);
    createdQuizId = response.body.data.quiz._id;
    expect(response.body.data.quiz).toMatchObject({
      _id: expect.any(String),
      title: 'techno',
      description: 'description of the quiz',
      createdAt: expect.any(String),
      isPublished: false,
      questions: [],
      updatedAt: expect.any(String),
    });
  });
  it('should update quiz', async () => {
    const response = await request(app).put(`/api/v1/quizzes/${createdQuizId}`).send({
      title: 'new quiz',
      description: 'description of the new quiz',
    });
    expect(response.status).toEqual(200);
    expect(response.body).toMatchObject({ message: 'Quiz updated successfully' })
    ;
  });

  it('should return the quiz with specified ID ', async () => {
    const response = await request(app).get(`/api/v1/quizzes/${createdQuizId}`);

    expect(response.status).toEqual(200);
    expect(response.body).toMatchObject({ message: 'Quiz founded' })
    ;
  });
  it('should delete the quiz with specified ID ', async () => {
    const response = await request(app).delete(`/api/v1/quizzes/${createdQuizId}`);

    expect(response.status).toEqual(200);
    expect(response.body).toMatchObject({ message: 'Quiz Deleted' });
  });
  it('updates the isPublished field', async () => {
    const response = await request(app)
      .patch(`/api/v1/quizzes/${createdQuizId}`)
    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ message: 'Quiz published successfully' });

  });
  it('updates the isPublished field', async () => {
    const response = await request(app)
      .patch(`/api/v1/quizzes/${createdQuizId}`)
    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ message: 'Quiz unpublished successfully' });
  });
  it('should get a list of all quizzes inside an array', async () => {
    const response = await request(app).get('/api/v1/quizzes');
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body.data)).toBe(true);

  });

});

