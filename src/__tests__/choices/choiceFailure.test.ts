import {describe, expect} from '@jest/globals';
import request from 'supertest';
import app from '../../app';
import {connectToTestingDb, disconnect} from '../../database';
import {Types} from 'mongoose';

let choiceId: Types.ObjectId;
const quizId = new Types.ObjectId()
const questionId = new Types.ObjectId()
let createdQuizId = new Types.ObjectId()
let createdQuestionId = new Types.ObjectId()
let createdChoiceId = new Types.ObjectId()
describe('failure choices endpoints ', () => {
    beforeAll(() => connectToTestingDb());
    afterAll(() => disconnect());
    it('should return an error in POST if the choice already exist in database', async () => {
        const response = await request(app).post('/api/v1/quizzes').send({
            title: 'first quiz',
            description: 'description of the quiz',
        });
        expect(response.status).toEqual(200);
      createdQuizId = response.body.data.quiz._id;
        const questionResponse = await request(app).post(`/api/v1/quizzes/${createdQuizId}/questions`).send({
            title: 'When Amir was born?',
            description: 'I need to know for my geography homework.',
            audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
            videolink: 'http://www.videowebsite.com/history/warTwoRecap',
            multipleChoice: false,
        });
        expect(questionResponse.status).toEqual(200);
      createdQuestionId = questionResponse.body.data.question._id;

        const choice = await request(app)
            .post(`/api/v1/quizzes/${createdQuizId}/questions/${createdQuestionId}/choices`)
            .send({
                answer: 'en 1998',
                isTrue: true,
            });
        expect(choice.status).toBe(200);
        createdChoiceId = choice.body.data.choice._id
        const res = await request(app)
            .post(`/api/v1/quizzes/${createdQuizId}/questions/${createdQuestionId}/choices`)
            .send({
                answer: 'en 1998',
                isTrue: true,
            });
        expect(res.status).toBe(400);
        expect(res.body).toMatchObject({message: 'this choice already exists', statusCode: 400});
    });
    it('should return an error for Joi validation if retrieving a choice by an invalid ID', async () => {
        const res = await request(app).get(`/api/v1/quizzes/${quizId}/questions/${questionId}/choices/${choiceId}`);
        expect(res.status).toBe(402);
        expect(res.body).toMatchObject({message: 'choiceId contains an invalid value', statusCode: 402});
    });

    it('should return an error if updating a choice with an invalid ID', async () => {
        const res = await request(app)
            .put(`/api/v1/quizzes/${quizId}/questions/${questionId}/choices/${choiceId}`)
            .send({
                answer: 'en 1998',
                isTrue: true,
            });
        expect(res.status).toBe(402);
        expect(res.body).toMatchObject({message: 'choiceId contains an invalid value', statusCode: 402});
    });

    it('should return an error if deleting a choice with an invalid ID', async () => {
        const res = await request(app).delete(
            `/api/v1/quizzes/${quizId}/questions/${questionId}/choices/${choiceId}`,
        );
        expect(res.status).toBe(402);
        expect(res.body).toMatchObject({message: 'choiceId contains an invalid value', statusCode: 402});
    });
});
