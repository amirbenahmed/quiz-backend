import request from 'supertest';
import app from '../../app';
import { Types } from 'mongoose';
import { expect } from '@jest/globals';
import { connectToTestingDb, disconnect } from '../../database';
let choiceId: Types.ObjectId;
let questionId: Types.ObjectId;
let quizId: Types.ObjectId;
describe('success choices endpoints', () => {
  beforeAll(() => connectToTestingDb());
  afterAll(() => disconnect());

  it('should create a quiz ', async () => {
    const response = await request(app).post('/api/v1/quizzes').send({
      title: 'first quiz',
      description: 'description of the quiz',
    });
    expect(response.status).toEqual(200);
    quizId = response.body.data.quiz._id;
  })
  it('should create a question ', async () => {
    const questionResponse = await request(app).post(`/api/v1/quizzes/${quizId}/questions`).send({
      title: 'When Amir was born?',
      description: 'I need to know for my geography homework.',
      audio: 'http://www.audiowebsite.com/history/warTwo.mp3',
      videolink: 'http://www.videowebsite.com/history/warTwoRecap',
      multipleChoice: false,
    });
    expect(questionResponse.status).toEqual(200);
    questionId = questionResponse.body.data.question._id;
  })

  it('should create a choice', async () => {
    const choiceResponse = await request(app)
      .post(`/api/v1/quizzes/${quizId}/questions/${questionId}/choices`)
      .send({
          answer: 'en 1998',
          isTrue: true,
      });

    expect(choiceResponse.status).toEqual(200);
    expect(choiceResponse.body.data.choice).toMatchObject({
      _id: expect.any(String),
      answer: 'en 1998',
      isTrue: true,
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
    });
    choiceId = choiceResponse.body.data.choice._id;
  });

  it('reads a choice', async () => {
    const response = await request(app).get(
      `/api/v1/quizzes/${quizId}/questions/${questionId}/choices/${choiceId}`,
    );

    expect(response.status).toBe(200);
    expect(response.body.data).toMatchObject({
      answer: 'en 1998',
      isTrue: true,
    });
  });

  it('updates a choice', async () => {
    const response = await request(app)
      .put(`/api/v1/quizzes/${quizId}/questions/${questionId}/choices/${choiceId}`)
      .send({
        answer: '1989?',
        isTrue: false,
      });

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({ message: 'Choice updated successfully' });
  });

  it('deletes a choice', async () => {
    const response = await request(app).delete(
      `/api/v1/quizzes/${quizId}/questions/${questionId}/choices/${choiceId}`,
    );

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({
      message: 'Choice Deleted',
      statusCode: 200,
    });
  });
});
