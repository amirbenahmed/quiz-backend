import { Request, Response } from 'express';
import asyncHandler from '../helpers/asyncHandler';
import QuizRepo from '../database/repository/QuizRepo';
import { BadRequestError, NotFoundError } from '../core/ApiError';
import { SuccessMsgResponse, SuccessResponse, SuccessResponsePagination } from '../core/ApiResponse';
import { Types } from 'mongoose';

export const createQuiz = asyncHandler(async (req: Request, res: Response) => {
  const checkQuiz = await QuizRepo.findByObj({ title: req.body.title });
  if (checkQuiz) {
    throw new BadRequestError('this quiz already exists');
  }
  const createdQuiz = await QuizRepo.create(req.body);
  return new SuccessResponse('Quiz created', createdQuiz).send(res);
});

export const updateQuiz = asyncHandler(async (req: Request, res: Response) => {
  const id = new Types.ObjectId(req.params.id);
  const quiz = await QuizRepo.findByObjAndUpdate(id, req.body);
  if (!quiz) {
    throw new NotFoundError('Quiz not found or deleted');
  }
  return new SuccessMsgResponse('Quiz updated successfully').send(res);
});

export const getQuizById = asyncHandler(async (req: Request, res: Response) => {
  const quiz = await QuizRepo.findByObj({
    _id: new Types.ObjectId(req.params.id),
  });
  if (!quiz) throw new NotFoundError('Quiz not found or deleted');
  return new SuccessResponse('Quiz founded', quiz).send(res);
});
export const deleteQuiz = asyncHandler(async (req: Request, res: Response) => {
  const quiz = await QuizRepo.findByObj({
    _id: new Types.ObjectId(req.params.id),
    deletedAt: null,
  });
  if (!quiz) throw new NotFoundError('Quiz not found or deleted');

  const deletedQuiz = await QuizRepo.deleteQuiz(quiz);
  return new SuccessResponse('Quiz Deleted', deletedQuiz).send(res);
});
export const setQuizStatus = asyncHandler(async (req: Request, res: Response) => {
  const id = new Types.ObjectId(req.params.id);

  const quiz = await QuizRepo.findByObj(id);
  if (!quiz) throw new NotFoundError('Quiz not found or deleted');

  if (quiz?.isPublished === true) {
    await QuizRepo.findByIdAndUnPublish(quiz._id);
    return new SuccessMsgResponse('Quiz unpublished successfully').send(res);
  }
  if (quiz?.isPublished === false) {
    await QuizRepo.findByIdAndPublish(quiz._id);
    return new SuccessMsgResponse('Quiz published successfully').send(res);
  }
});
export const getQuizzes = asyncHandler(async (req: Request, res: Response) => {
  const { page, perPage, deleted } = req.query;
  const options = {
    page: parseInt(page as string, 10) || 1,
    limit: parseInt(perPage as string, 10) || 10,
  };

  const quizzes = await QuizRepo.findAll(options, req.query, {
    isPaging: true,
    deleted: deleted == 'true' ? true : false
  });

  const { docs, ...meta } = quizzes;
  new SuccessResponsePagination('All quizzes returned successfully',
    docs,
    meta
  ).send(res);

});