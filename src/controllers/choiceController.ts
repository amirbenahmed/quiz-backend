import { Request, Response } from 'express';
import asyncHandler from '../helpers/asyncHandler';
import ChoiceRepo from '../database/repository/ChoiceRepo';
import { BadRequestError, NotFoundError } from '../core/ApiError';
import { SuccessMsgResponse, SuccessResponse } from '../core/ApiResponse';
import { Types } from 'mongoose';
import QuestionRepo from '../database/repository/QuestionRepo';
import QuizRepo from '../database/repository/QuizRepo';

export const createChoice = asyncHandler(async (req: Request, res: Response) => {
  const questionId = new Types.ObjectId(req.params.questionId);
  const quizId = new Types.ObjectId(req.params.quizId);
  const checkChoice = await ChoiceRepo.findByObj({ answer: req.body.answer });
  if (checkChoice) {
    throw new BadRequestError('this choice already exists');
  }

  const checkQuiz = await QuizRepo.findByObj(quizId);
  if (!checkQuiz) {
    throw new BadRequestError('this quiz does not exist');
  }

  const checkQuestion = await QuestionRepo.findByObj(questionId);
  if (!checkQuestion) {
    throw new BadRequestError('this question does not exist');
  }


  const createdChoice = await ChoiceRepo.create({ ...req.body });

  await QuestionRepo.addChoiceToQuestion(questionId, createdChoice.choice._id);
  return new SuccessResponse('Choice created', createdChoice).send(res);
});

export const updateChoice = asyncHandler(async (req: Request, res: Response) => {
  const questionId = new Types.ObjectId(req.params.questionId);
  const choiceId = new Types.ObjectId(req.params.choiceId);
  const quizId = new Types.ObjectId(req.params.questionId);

  await relationChecks(quizId, questionId, choiceId);
  const choice = await ChoiceRepo.findByObjAndUpdate(choiceId, req.body);

  if (!choice) {
    throw new NotFoundError('No Choice found with that id');
  }
  return new SuccessMsgResponse('Choice updated successfully').send(res);
});

export const getChoiceById = asyncHandler(async (req: Request, res: Response) => {
  const questionId = new Types.ObjectId(req.params.questionId);
  const choiceId = new Types.ObjectId(req.params.choiceId);
  const quizId = new Types.ObjectId(req.params.questionId);
  const choice = await ChoiceRepo.findById(choiceId);

  if (!choice) throw new BadRequestError('Choice not found or deleted');
  await relationChecks(quizId, questionId, choiceId);


  return new SuccessResponse('Choice founded', choice).send(res);
});
export const deleteChoice = asyncHandler(async (req: Request, res: Response) => {
  const choiceId = new Types.ObjectId(req.params.choiceId);
  const questionId = new Types.ObjectId(req.params.questionId);
  const quizId = new Types.ObjectId(req.params.questionId);

  await relationChecks(quizId, questionId, choiceId);

  const exist = await QuestionRepo.isChoiceExist( questionId, choiceId );

  if (!exist) {
    throw new BadRequestError('this choice does not belong to the question');
  }

  const choice = await ChoiceRepo.findById(choiceId);
  if (!choice) throw new BadRequestError('Choice not found or deleted');

  const deletedChoice = await ChoiceRepo.deleteChoice(choice);

  await QuestionRepo.deleteChoiceFromQuestion(questionId, deletedChoice._id);

  return new SuccessResponse('Choice Deleted', deletedChoice).send(res);
});

async function relationChecks (quizId: Types.ObjectId, questionId: Types.ObjectId, choiceId: Types.ObjectId){
  const checkQuiz = await QuizRepo.findByObj(quizId);
  if (!checkQuiz) {
    throw new BadRequestError('this quiz does not exist');
  }
  const checkQuestion = await QuestionRepo.findByObj(questionId);
  if (!checkQuestion) {
    throw new BadRequestError('this question does not exist');
  }
  const exist = await QuestionRepo.isChoiceExist(questionId, choiceId);
  if (!exist) {
    throw new BadRequestError('this choice does not belong to the question');
  }
}