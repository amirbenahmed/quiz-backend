import { Request, Response } from 'express';
import asyncHandler from '../helpers/asyncHandler';
import QuestionRepo from '../database/repository/QuestionRepo';
import { BadRequestError, NotFoundError } from '../core/ApiError';
import { SuccessMsgResponse, SuccessResponse } from '../core/ApiResponse';
import { Types } from 'mongoose';
import QuizRepo from '../database/repository/QuizRepo';

export const createQuestion = asyncHandler(async (req: Request, res: Response) => {
  const quizId = new Types.ObjectId(req.params.quizId);
  const checkQuestion = await QuestionRepo.findByObj({ title: req.body.title });
  const checkQuiz = await QuizRepo.findByObj({ _id: quizId, deletedAt:null });
  if (!checkQuiz) {
    throw new BadRequestError('this quiz does not exist');
  }

  if (checkQuestion) {
    throw new BadRequestError('this question already exists');
  }

  const createdQuestion = await QuestionRepo.create({ ...req.body, image: req.file?.filename });

  await QuizRepo.addOneQuestion(quizId, createdQuestion.question._id);
  return new SuccessResponse('Question created', createdQuestion).send(res);
});

export const updateQuestion = asyncHandler(async (req: Request, res: Response) => {
  const questionId = new Types.ObjectId(req.params.questionId);
  const quizId = new Types.ObjectId(req.params.quizId);

  const question = await QuestionRepo.findByObjAndUpdate(questionId, req.body);
  if (!question) {
    throw new NotFoundError('Question not found or deleted');
  }
  const exist = await QuizRepo.isQuestionExist(quizId, questionId);
  if (!exist) {
    throw new BadRequestError('this question does not belong to the quiz');
  }

  return new SuccessMsgResponse('Question updated successfully').send(res);
});

export const getQuestionById = asyncHandler(async (req: Request, res: Response) => {
  const questionId = new Types.ObjectId(req.params.questionId);
  const quizId = new Types.ObjectId(req.params.quizId);

  const [question, exist] = await Promise.all([
    QuestionRepo.findByObj({ _id: questionId, deletedAt: null }),
    QuizRepo.isQuestionExist(quizId, questionId),
  ]);

  // const exist = await QuizRepo.isQuestionExist(quizId,questionId);
  if (!exist) {
    throw new BadRequestError('this question does not belong to the quiz');
  }

  if (!question) throw new NotFoundError('Question not found or deleted');
  return new SuccessResponse('Question founded', question).send(res);
});
export const deleteQuestion = asyncHandler(async (req: Request, res: Response) => {
  const quizId = new Types.ObjectId(req.params.quizId);
  const questionId = new Types.ObjectId(req.params.questionId);

  const exist = await QuizRepo.isQuestionExist(quizId, questionId);
  if (!exist) {
    throw new BadRequestError('this question does not belong to the quiz');
  }

  const question = await QuestionRepo.findByObj({
    _id: questionId,
    deletedAt: null,
  });

  if (!question) throw new NotFoundError('Question not found or deleted');

  const deletedQuestion = await QuestionRepo.deleteQuestion(question);
  await QuizRepo.deleteOneQuestion(quizId, deletedQuestion._id);
  return new SuccessResponse('Question Deleted', deletedQuestion).send(res);
});

export const getQuestions = asyncHandler(async (req: Request, res: Response) => {
  const { page, perPage, deleted } = req.query;
  const options = {
    page: parseInt(page as string, 10) || 1,
    limit: parseInt(perPage as string, 10) || 10,
  };

  const questions = await QuestionRepo.findAll(options, req.query, {
    isPaging: true,
    deleted: deleted == 'true' ? true : false,
  });
  const lastWeek = new Date(Date.now() - 24 * 60 * 60 * 1000);
  const { docs, ...meta } = questions;
  let last: any;
  docs.map((i) => {
    // @ts-ignore
    last = i.createdAt >= lastWeek;
  });
  if (!last) throw new NotFoundError('there is not questions last week');
  new SuccessResponse('All questions returned successfully', {
    last,
    meta,
  }).send(res);
});
