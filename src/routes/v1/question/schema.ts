import Joi from '@hapi/joi';
import { JoiObjectId } from '../../../helpers/validator';

export default {
  /*
   * @swagger
   * components:
   *   securitySchemes:
   *      bearerAuth:
   *          type: http
   *          scheme: bearer
   *          bearerFormat: JWT
   */

  /**
   * @swagger
   * components:
   *   schemas:
   *     Question:
   *       type: object
   *       properties:
   *         title:
   *           type: string
   *           description: The question title
   *         description:
   *           type: string
   *           description: question description
   *         choices:
   *           type: array
   *           description: choices belongs to question
   *         image:
   *           type: string
   *           description: image related to the question
   *         audio:
   *           type: string
   *           description: audio related to the question
   *         videolink:
   *           type: string
   *           description: video link related to the question
   *     CreateQuestion:
   *       type: object
   *       required:
   *         - title
   *         - description
   *       properties:
   *         title:
   *           type: string
   *           description: The title of the question
   *         description:
   *           type: string
   *           description: The description of the question
   *     UpdateQuestion:
   *       type: object
   *       properties:
   *         title:
   *           type: string
   *           description: The title of the question
   *         description:
   *           type: string
   *           description: The description of the question
   */
  question: Joi.object({
    title: Joi.string().min(3).required(),
    description: Joi.string().min(3).required(),
    videolink: Joi.string().optional().uri(),
    audio: Joi.string().optional().uri(),
    multipleChoice: Joi.boolean().optional(),
    answers:Joi.array().optional(),
  }),
  checkId: Joi.object({
    quizId: JoiObjectId().required(),
  }),
  checkIds: Joi.object({
    questionId: JoiObjectId().required(),
    quizId: JoiObjectId().required(),
  }),
  updateQuestion: Joi.object({
    title: Joi.string().trim().min(3).optional(),
    description: Joi.string().min(3).optional(),
    image: Joi.string().optional().uri(),
    audio: Joi.string().optional().uri(),
    videolink: Joi.string().optional().uri(),
    multipleChoice: Joi.boolean().optional(),
    answers:Joi.array().optional(),
  }),
};
