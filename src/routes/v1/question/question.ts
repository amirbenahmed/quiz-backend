import express from 'express';
import validator, { ValidationSource } from '../../../helpers/validator';
import schema from './schema';
import {
  createQuestion,
  deleteQuestion,
  getQuestionById,
  updateQuestion,
} from '../../../controllers/questionController';
import { upload } from '../../../utils/fileUploadOptions';
const router = express.Router();

/**
 * @swagger
 * /quizzes/{quizId}/questions/ :
 *  post:
 *    summary: Upload Image
 *    tags: [Question]
 *    parameters:
 *       - in: path
 *         name: quizId
 *         schema:
 *              type: string
 *         required: true
 *         description: quizId
 *    requestBody:
 *        required: true
 *        content:
 *          multipart/form-data:
 *            schema:
 *              type: object
 *              required:
 *                - image
 *                - title
 *              properties:
 *               image:
 *                  type: string
 *                  format: binary
 *               title:
 *                  type: string
 *               description:
 *                  type: string
 *    responses:
 *      200:
 *          description: Image successfully uploaded
 *          content:
 *            application/json:
 *              schema:
 *                  $ref: '#/components/schemas/Question'
 *      400:
 *        description:  Validation Failed
 *      401:
 *        description: Error Token
 *      403:
 *        description: Access Denied / Unauthorized
 *      500:
 *        description: Internal server error
 */
router.post('/:quizId/questions',
  validator(schema.checkId, ValidationSource.PARAM),
    upload.single('image'),
 // validator(schema.question),
  createQuestion,

  );
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}:
 *   put:
 *     summary: update Question
 *     parameters:
 *        - in: path
 *          name: quizId
 *          schema:
 *            type: string
 *        - in: path
 *          name: questionId
 *          schema:
 *            type: string
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/CreateQuestion'
 *     tags: [Question]
 *     responses:
 *       200:
 *         description: Question creation
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Question'
 *
 */

router.put('/:quizId/questions/:questionId',
  validator(schema.checkIds, ValidationSource.PARAM), validator(schema.updateQuestion), updateQuestion);
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}:
 *   get:
 *     summary: Get one question by id
 *     tags: [Question]
 *     parameters:
 *      - in: path
 *        name: quizId
 *      - in: path
 *        name: questionId
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Question'
 */
router.get('/:quizId/questions/:questionId',
  validator(schema.checkIds, ValidationSource.PARAM), getQuestionById);
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}:
 *   delete:
 *     summary: Delete question by id
 *     tags: [Question]
 *     parameters:
 *        - in: path
 *          name: quizId
 *          schema:
 *            type: string
 *        - in: path
 *          name: questionId
 *          schema:
 *            type: string
 *     responses:
 *       200:
 *         description: Question deleted
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Question'
 *
 */
router.delete('/:quizId/questions/:questionId',
  validator(schema.checkIds, ValidationSource.PARAM), deleteQuestion);

export default router;
