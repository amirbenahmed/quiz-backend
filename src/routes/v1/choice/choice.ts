import express from 'express';
import validator, { ValidationSource } from '../../../helpers/validator';
import schema from './schema';
import { createChoice, deleteChoice, getChoiceById, updateChoice } from '../../../controllers/choiceController';

const router = express.Router();
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}/choices:
 *   post:
 *     summary: Choice Creation
 *     tags: [Choice]
 *     parameters:
 *      - in: path
 *        name: quizId
 *      - in: path
 *        name: questionId
 *     requestBody:
 *         required: true
 *         content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/CreateChoice'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Choice'
 */
router.post('/:quizId/questions/:questionId/choices',
  validator(schema.checkId, ValidationSource.PARAM),
  createChoice);
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}/choices/{choiceId}:
 *   put:
 *     summary: Update the choice
 *     tags: [Choice]
 *     parameters:
 *      - in: path
 *        name: quizId
 *      - in: path
 *        name: questionId
 *      - in: path
 *        name: choiceId
 *     requestBody:
 *         required: true
 *         content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/UpdateChoice'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Choice'
 */

router.put('/:quizId/questions/:questionId/choices/:choiceId',
    validator(schema.checkIds, ValidationSource.PARAM),
    validator(schema.updateChoice),
  updateChoice);
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}/choices/{choiceId}:
 *   get:
 *     summary: Get one choice
 *     tags: [Choice]
 *     parameters:
 *      - in: path
 *        name: quizId
 *      - in: path
 *        name: questionId
 *      - in: path
 *        name: choiceId
 *     requestBody:
 *         required: true
 *         content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/UpdateChoice'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Choice'
 */
router.get('/:quizId/questions/:questionId/choices/:choiceId',
  validator(schema.checkIds, ValidationSource.PARAM),
  getChoiceById);
/**
 * @swagger
 * /quizzes/{quizId}/questions/{questionId}/choices/{choiceId}:
 *   delete:
 *     summary: Delete one choice
 *     tags: [Choice]
 *     parameters:
 *      - in: path
 *        name: quizId
 *      - in: path
 *        name: questionId
 *      - in: path
 *        name: choiceId
 *     requestBody:
 *         required: true
 *         content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/UpdateChoice'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Choice'
 */
router.delete('/:quizId/questions/:questionId/choices/:choiceId',
  validator(schema.checkIds, ValidationSource.PARAM), deleteChoice);

export default router;
