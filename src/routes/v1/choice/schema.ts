import Joi from '@hapi/joi';
import { JoiObjectId } from '../../../helpers/validator';

export default {
  /*
   * @swagger
   * components:
   *   securitySchemes:
   *      bearerAuth:
   *          type: http
   *          scheme: bearer
   *          bearerFormat: JWT
   */

  /**
   * @swagger
   * components:
   *   schemas:
   *     Choice:
   *       type: object
   *       properties:
   *         answer:
   *           type: string
   *           description: The choice title
   *         isTrue:
   *           type: boolean
   *           description: valid choice or not
   *           default: false
   *     CreateChoice:
   *       type: object
   *       required:
   *         - answer
   *         - isTrue
   *       properties:
   *         answer:
   *           type: string
   *           description: The answer of the choice
   *         isTrue:
   *           type: boolean
   *           description: valid choice or not
   *           default: false
   *     UpdateChoice:
   *       type: object
   *       properties:
   *         answer:
   *           type: string
   *           description: The answer of the choice
   *         isTrue:
   *           type: boolean
   *           description: valid choice or not
   *           default: false
   */
  choice: Joi.object({
    answer: Joi.string().trim().min(3).required(),
    isTrue: Joi.boolean().required()

  }),
  checkId: Joi.object({
    quizId: JoiObjectId().required(),
    questionId: JoiObjectId().required(),
  }),
  checkIds: Joi.object({
    quizId: JoiObjectId().required(),
    choiceId: JoiObjectId().required(),
    questionId: JoiObjectId().required(),
  }),
  updateChoice: Joi.object({
    answer: Joi.string().trim().min(3).optional(),
    isTrue: Joi.boolean().optional(),
  }),
};
