import express from 'express';
import quiz from './quiz/quiz';
import question from './question/question';
import choice from './choice/choice';
const router = express.Router();
router.use('/quizzes', question);
router.use('/quizzes', quiz);
router.use('/quizzes', choice);

export default router;
