import Joi from '@hapi/joi';
import { JoiObjectId } from '../../../helpers/validator';

export default {
  /*
   * @swagger
   * components:
   *   securitySchemes:
   *      bearerAuth:
   *          type: http
   *          scheme: bearer
   *          bearerFormat: JWT
   */

  /**
   * @swagger
   * components:
   *   schemas:
   *     Quiz:
   *       type: object
   *       properties:
   *         title:
   *           type: string
   *           description: The quiz title
   *         description:
   *           type: string
   *           description: quiz description
   *         isPublished:
   *           type: boolean
   *           description: indicates if the quiz is published or not
   *           default: false
   *         questions:
   *           type: array
   *           description: questions belongs to quiz
   *     CreateQuiz:
   *       type: object
   *       required:
   *         - title
   *         - description
   *       properties:
   *         title:
   *           type: string
   *           description: The title of the quiz
   *         description:
   *           type: string
   *           description: The description of the quiz
   *     UpdateQuiz:
   *       type: object
   *       properties:
   *         title:
   *           type: string
   *           description: The title of the quiz
   *         description:
   *           type: string
   *           description: The description of the quiz
   *     PublishUnpublishQuiz:
   *       type: object
   *       properties:
   *         isPublished:
   *           type: boolean
   *           description: is the quiz published or unpublished
   */
  quiz: Joi.object({
    title: Joi.string().trim().min(3).max(30).required(),
    description: Joi.string().min(3).required(),
    category:Joi.string().min(3).optional(),
    isPublished:Joi.boolean().optional(),
    questions:Joi.array().optional(),

  }),
  checkQuizId: Joi.object({
    id: JoiObjectId().required(),
  }),
  updateQuiz: Joi.object({
    title: Joi.string().trim().min(3).max(30).required(),
    description: Joi.string().min(3).required(),
    category:Joi.string().min(3).optional(),
    isPublished:Joi.boolean().optional(),
    questions:Joi.array().optional(),
  }),
};
