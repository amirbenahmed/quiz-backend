import express from 'express';
import {
  createQuiz,
  deleteQuiz,
  getQuizById, getQuizzes, setQuizStatus,
  updateQuiz,
} from '../../../controllers/quizController';
import validator, { ValidationSource } from '../../../helpers/validator';
import schema from './schema';
const router = express.Router();
/**
 * @swagger
 * /quizzes:
 *   post:
 *     summary: Quiz Creation
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/CreateQuiz'
 *     tags: [Quiz]
 *     responses:
 *       200:
 *         description: Quiz creation
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Quiz'
 *
 */
router.post('/', validator(schema.quiz),createQuiz);
/**
 * @swagger
 * /quizzes/{id}:
 *   put:
 *     summary: update Quiz
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/UpdateQuiz'
 *     tags: [Quiz]
 *     responses:
 *       200:
 *         description: Update Quiz
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Quiz'
 *
 */

router.put('/:id', validator(schema.checkQuizId, ValidationSource.PARAM),validator(schema.updateQuiz), updateQuiz);
/**
 * @swagger
 * /quizzes/{id}:
 *   get:
 *     summary: Get one quiz by id
 *     tags: [Quiz]
 *     parameters:
 *      - in: path
 *        name: id
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Quiz'
 */
router.get('/:id', validator(schema.checkQuizId, ValidationSource.PARAM), getQuizById);
/**
 * @swagger
 * /quizzes/{id}:
 *   delete:
 *     summary: Delete quiz by id
 *     tags: [Quiz]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *     responses:
 *       200:
 *         description: Quiz deleted
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Quiz'
 *
 */
router.delete('/:id', validator(schema.checkQuizId, ValidationSource.PARAM), deleteQuiz);
/**
 * @swagger
 * /quizzes/{id}:
 *   patch:
 *     summary: publish or unpublish one quiz
 *     tags: [Quiz]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *     responses:
 *       200:
 *         description: Quiz published/unpublished
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PublishUnpublishQuiz'
 *
 */
router.patch('/:id', validator(schema.checkQuizId, ValidationSource.PARAM), setQuizStatus);
/**
 * @swagger
 * /quizzes:
 *   get:
 *     summary: Get all quizzes
 *     tags: [Quiz]
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *               $ref: '#/components/schemas/Quiz'
 */
router.get('/', getQuizzes)
export default router;
