import { Types } from 'mongoose';
import Quiz, { QuizModel } from '../models/Quiz';
import { PaginationModel } from 'mongoose-paginate-ts';
import APIFeatures from '../../helpers/ApiFeature';
import { ApiOptions } from 'api-request';
import { NotFoundError } from '../../core/ApiError';

type pagingObj = {
  limit: number;
  page: number;
};
export default class QuizRepo {
  public static async create(quiz: Quiz): Promise<{ quiz: Quiz }> {
    const now = new Date();
    quiz.createdAt = quiz.updatedAt = now;
    const createdQuiz = await QuizModel.create(quiz);
    return { quiz: createdQuiz.toObject() };
  }

  public static findByObj(obj: object): Promise<Quiz | null> {
    return QuizModel.findOne({ ...obj, deleteAt:null }).lean<Quiz>().exec();
  }


  static findByObjAndUpdate(id: Types.ObjectId, quiz: Quiz) {
    return QuizModel.findOneAndUpdate({ _id: id, deletedAt: null },
      { $set: { ...quiz } }, { new: true });
  }

  public static async deleteQuiz(quiz: Quiz): Promise<any> {
    quiz.updatedAt = new Date();
    let title = quiz.title as string;
    let regex = '^old[0-9]+' + title;
    const deletedQuiz = await QuizModel.count({ title: { $regex: regex } });
    return QuizModel.findByIdAndUpdate(
      quiz._id,
      { $set: { title: `old${deletedQuiz}${title}`, deletedAt: Date.now() } },
      { new: true },
    ).lean()
      .exec();

  }

  public static findByIdAndPublish(id: Types.ObjectId) {
    return QuizModel.findByIdAndUpdate(id, { $set: { isPublished: true } });

  }

  public static findByIdAndUnPublish(id: Types.ObjectId) {
    return QuizModel.findByIdAndUpdate(id, { $set: { isPublished: false } });

  }

  public static async findAll(paging: pagingObj, query: object, apiOptions: ApiOptions): Promise<PaginationModel<Quiz>> {
    let findAllQuery = apiOptions.deleted
      ? QuizModel.find({ deletedAt: { $ne: null } })
      : QuizModel.find({ deletedAt: null });

    const features = new APIFeatures(
      findAllQuery,
      query,
    )
      .filter()
      .sort()
      .limitFields()
      .search(['title', 'description']);

    const options = {
      query: features.query,
      limit: paging.limit ? paging.limit : null,
      page: paging.page ? paging.page : null,
    };

    return await QuizModel.paginate(options) as PaginationModel<Quiz>;

  }

  public static async findById(id: Types.ObjectId) {
    const quiz = await QuizModel.findById({ id, deleteAt:null });
    if (!quiz) throw new NotFoundError('Quiz not found or deleted');
    return quiz;
  }
  public static async deleteOneQuestion(quizId: Types.ObjectId, questionId:any){
    return QuizModel.findOneAndUpdate({_id: quizId, deleteAt:null }, {
      $pull: { questions: questionId }
    });
  }

  public static async addOneQuestion(quizId: Types.ObjectId, questionId:any){
    return QuizModel.findOneAndUpdate({_id: quizId, deleteAt:null }, {
      $push: { questions: questionId }
    });
  }
  public static async isQuestionExist(quizId: Types.ObjectId, questionId: Types.ObjectId){
     return QuizModel.findOne({ _id: quizId, questions: questionId, deleteAt:null  });
  }

}
