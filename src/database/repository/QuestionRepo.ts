import { Types } from 'mongoose';
import Question, { QuestionModel } from '../models/Question';
import { ApiOptions } from 'api-request';
import { PaginationModel } from 'mongoose-paginate-ts';
import Quiz from '../models/Quiz';
import APIFeatures from '../../helpers/ApiFeature';

type pagingObj = {
  limit: number;
  page: number;
};
export default class QuestionRepo {
  public static async create(question: Question): Promise<{ question: Question }> {
    const now = new Date();
    question.createdAt = question.updatedAt = now;
    const createdQuestion = await QuestionModel.create(question);
    return { question: createdQuestion.toObject() };
  }

  public static findByObj(obj: object): Promise<Question | null> {
    return QuestionModel.findOne({ ...obj, deletedAt: null }).lean<Question>().exec();
  }

  static findByObjAndUpdate(id: Types.ObjectId, question: Question) {
    return QuestionModel.findOneAndUpdate({ _id: id, deletedAt: null }, { $set: { ...question } }, { new: true });
  }

  public static async deleteQuestion(question: Question): Promise<any> {
    question.updatedAt = new Date();
    let title = question.title as string;
    let regex = '^old[0-9]+' + title;
    const deletedQuestion = await QuestionModel.count({ title: { $regex: regex } });
    return QuestionModel.findByIdAndUpdate(
      question._id,
      { $set: { title: `old${deletedQuestion}${title}`, deletedAt: Date.now() } },
      { new: true },
    ).lean()
      .exec();

  }

  public static async findAll(paging: pagingObj, query: object, apiOptions: ApiOptions): Promise<PaginationModel<Quiz>> {
    let findAllQuery = apiOptions.deleted
      ? QuestionModel.find({ deletedAt: { $ne: null } })
      : QuestionModel.find({ deletedAt: null });

    const features = new APIFeatures(
      findAllQuery,
      query,
    )
      .filter()
      .sort()
      .limitFields()
      .search(['title', 'description']);

    const options = {
      query: features.query,
      limit: paging.limit ? paging.limit : null,
      page: paging.page ? paging.page : null,
    };

    return await QuestionModel.paginate(options) as PaginationModel<Quiz>;

  }

  static findById(id: Types.ObjectId) {
    return QuestionModel.findById({ _id: id, deleteAt: null });
  }

  public static async isChoiceExist(questionId: Types.ObjectId, choiceId: Types.ObjectId) {
    return QuestionModel.findOne({ _id: questionId, answers: choiceId, deleteAt:null  });
  }

  public static async addChoiceToQuestion(questionId: Types.ObjectId, choiceId: Types.ObjectId) {
    return QuestionModel.findOneAndUpdate({ _id: questionId }, {
      $push: { answers: choiceId },
    });
  }

  public static async deleteChoiceFromQuestion(questionId: Types.ObjectId, choiceId: any) {
    return QuestionModel.findOneAndUpdate({ _id: questionId, deletedAt: null }, {
      $pull: { answers: choiceId },
    });
  }


}
