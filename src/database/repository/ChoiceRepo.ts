import { Types } from 'mongoose';
import Choice, { ChoiceModel } from '../models/Choice';
import { ApiOptions } from 'api-request';
import { PaginationModel } from 'mongoose-paginate-ts';
import APIFeatures from '../../helpers/ApiFeature';

type pagingObj = {
  limit: number;
  page: number;
};
export default class ChoiceRepo {
  public static async create(choice: Choice): Promise<{ choice: Choice }> {
    const now = new Date();
    choice.createdAt = choice.updatedAt = now;
    const createdChoice = await ChoiceModel.create(choice);
    return { choice: createdChoice.toObject() };
  }

  public static findByObj(obj: object): Promise<Choice | null> {
    return ChoiceModel.findOne({...obj, deletedAt:null }).lean<Choice>().exec();
  }
  public static findById(id: Types.ObjectId): Promise<Choice | null> {
    return ChoiceModel.findOne({
      _id: id
    }).lean<Choice>().exec();
  }
  static findByObjAndUpdate(id: Types.ObjectId, choice: Choice) {
    return ChoiceModel.findOneAndUpdate({ _id: id, deletedAt:null }, { $set: { ...choice } }, { new: true });
  }

  public static async deleteChoice(choice: Choice): Promise<any> {
    choice.updatedAt = new Date();
    let answer = choice.answer as string;
    let regex = '^old[0-9]+' + answer;
    const deletedChoice = await ChoiceModel.count({ answer: { $regex: regex } });
    return ChoiceModel.findByIdAndUpdate(
      choice._id,
      { $set: { answer: `old${deletedChoice}${answer}`, deletedAt: Date.now() } },
      { new: true },
    ).lean()
      .exec();

  }

  public static async findAll(paging: pagingObj, query: object, apiOptions: ApiOptions): Promise<PaginationModel<Choice>> {
    let findAllQuery = apiOptions.deleted
      ? ChoiceModel.find({ deletedAt: { $ne: null } })
      : ChoiceModel.find({ deletedAt: null });

    const features = new APIFeatures(
      findAllQuery,
      query,
    )
      .filter()
      .sort()
      .limitFields()
      .search(['answer', 'isTrue']);

    const options = {
      query: features.query,
      limit: paging.limit ? paging.limit : null,
      page: paging.page ? paging.page : null,
    };

    return await ChoiceModel.paginate(options) as PaginationModel<Choice>;

  }
}
