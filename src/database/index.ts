import mongoose from 'mongoose';
import * as dotenv from 'dotenv';
import { dbParams } from '../utils/dbParams';
import Logger from '../core/Logger';
dotenv.config();
// Build the connection string

Logger.debug(dbParams.dbURI);

// Create the database connection
export async function connectToTestingDb() {
  mongoose
    .connect(dbParams.testingDB)
    .then(() => {
      Logger.info('Mongoose connection done for testing db');
    })
    .catch((e) => {
      Logger.info('Mongoose connection error');
      Logger.error(e);
    });
}

mongoose
  .connect(dbParams.dbURI)
  .then(() => {
    Logger.info('Mongoose connection done');
  })
  .catch((e) => {
    Logger.info('Mongoose connection error');
    Logger.error(e);
  });

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', () => {
  Logger.info('Mongoose default connection open to ' + dbParams.dbURI);
});

// If the connection throws an error
mongoose.connection.on('error', (err) => {
  Logger.error('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  Logger.info('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    Logger.info('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

export async function disconnect(): Promise<void> {
  return mongoose.connection.close();
}
