import { Document, model, Schema } from 'mongoose';
import Question from './Question';
import { mongoosePagination, Pagination } from 'mongoose-paginate-ts';

export const DOCUMENT_NAME = 'Quiz';
export const COLLECTION_NAME = 'quiz';
export default interface Quiz extends Document {
  title: string;
  description: string;
  category?: string;
  isPublished?: boolean;
  questions?: Question[];
  createdAt?: Date;
  updatedAt?: Date;
}
const schema = new Schema(
  {
    title: {
      type: Schema.Types.String,
      required: true,
    },
    description: {
      type: Schema.Types.String,
      required: true,
    },
    category: {
      type: Schema.Types.String,
    },
    isPublished: {
      type: Schema.Types.Boolean,
      default: false,
    },
    questions: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Question',
      },
    ],
    createdAt: {
      type: Date,
      required: true,
      select: false,
    },
    updatedAt: {
      type: Date,
      required: true,
      select: false,
    },
    deletedAt: {
      type: Date,
      select: true,
    },
  },
  {
    versionKey: false,
    timestamps: true
  },
);
schema.plugin(mongoosePagination);
schema.pre('findOne', function() {
    this.populate({
        path: 'questions',
        populate: {
            path: 'answers'
        }
    });
})
export const QuizModel = model<Quiz, Pagination<Quiz>>(DOCUMENT_NAME, schema, COLLECTION_NAME);
