import { Document, model, Schema } from 'mongoose';
import { mongoosePagination, Pagination } from 'mongoose-paginate-ts';
import Question from './Question';

export const DOCUMENT_NAME = 'Choice';
export const COLLECTION_NAME = 'choice';
export default interface Choice extends Document {
  answer: string;
  isTrue: boolean;
  deletedAt: Date;
  createdAt: Date;
  updatedAt:Date;
}
const schema = new Schema(
  {
    answer: {
      type: Schema.Types.String,
      required: true,
    },
    isTrue: {
      type: Schema.Types.Boolean,
      default: false,
    },
    createdAt: {
      type:Schema.Types.Date,
      select:false
    },
    deleteAt:{
      type:Schema.Types.Date,
      select:false
    },
    updatedAt:{
      type:Schema.Types.Date,
      select:false
    }
  },
  {
    versionKey: false,
    timestamps:true
  },
);
schema.plugin(mongoosePagination);
export const ChoiceModel = model<Choice, Pagination<Choice>>(DOCUMENT_NAME, schema, COLLECTION_NAME);
