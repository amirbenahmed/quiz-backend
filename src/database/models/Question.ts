import { Document, model, Schema } from 'mongoose';
import Choice from './Choice';
import { mongoosePagination, Pagination } from 'mongoose-paginate-ts';

export const DOCUMENT_NAME = 'Question';
export const COLLECTION_NAME = 'question';
export default interface Question extends Document {
  title: string;
  description: string;
  image?: string;
  audio?: string;
  videolink?: string;
  multipleChoice: boolean;
  answers: Choice[];
  createdAt?: Date;
  updatedAt?: Date;
  deleteAt?: Date;
}
const schema = new Schema(
  {
    title: {
      type: Schema.Types.String,
      unique: true,
      required: true,
    },
    description: {
      type: Schema.Types.String,
      required: true,
    },
    image: {
      type: Schema.Types.String,
    },
    audio: {
      type: Schema.Types.String,
    },
    videolink: {
      type: Schema.Types.String,
    },
    multipleChoice: {
      type: Schema.Types.Boolean,
      default: false,
    },
    answers: {
      type: [
        {
          type: Schema.Types.ObjectId,
          ref: 'Choice',
        },
      ],
      select: false,
    },
    createdAt: {
      type: Date,
      required: true,
      select: false,
    },
    updatedAt: {
      type: Date,
      required: true,
      select: false,
    },
    deletedAt: {
      type:Date,
      select:false
    }
  },
  {
    versionKey: false,
  },
);
schema.plugin(mongoosePagination);
schema.pre('findOne', function() {
  this.populate("answers")
})
export const QuestionModel = model<Question, Pagination<Question>>(DOCUMENT_NAME, schema, COLLECTION_NAME);
