import Logger from '../core/Logger';
import { db } from '../config';
import * as dotenv from 'dotenv';
dotenv.config();


const dbURI = `mongodb://${db.host}:${db.port}/${db.name}`;
const testingDB = process.env.DB_TEST || '';
Logger.debug(dbURI);
export const dbParams = {
  dbURI: dbURI,
  testingDB: testingDB,
};

