import multer from 'multer';
import { Request } from 'express';
import path from 'path';
const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__dirname,'../public/images'));
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' + file.originalname);
  },
});
const supportedMimeTypes = ['application/pdf', 'image/jpeg', 'image/png', 'image/gif', 'image/bnp'];
const multerFilter = (req: Request, file: any, cb: any) => {
  if (!supportedMimeTypes.includes(file.mimetype)) {
    cb("please only images or pdf", false);

  }
  cb(null, true);
};

export const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
});
